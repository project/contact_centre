
CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------

Current Maintainer: Scot Hubbard - http://drupal.org/user/868214

This module captures all message submitted via the site-wide contact
form and allow administrators with the "administer contact centre" to
view the contact message and also record notes against them, which can
optionally be sent to the original sender via email.  It also has full
views integration.


INSTALLATION
------------

Install as usual...
see http://drupal.org/documentation/install/modules-themes/modules-7


CONFIGURATION
-------------

Once installed, use the "configure" link on the modules page to go to
the configuration page for this module.

SIMILAR MODULES
---------------

Contact Save (https://drupal.org/project/contact_save)
