<?php
/**
 * @file
 * Install, update, and uninstall functions for the contact_centre module.
 */

/**
 * Implements hook_schema().
 */
function contact_centre_schema() {
  $schema['contact_centre_messages'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The id of the contact message',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'cid' => array(
        'description' => 'The ID of the contact category',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'uid' => array(
        'description' => 'The ID of the user leaving the contact message',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'description' => 'The name of the user leaving the contact message',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'mail' => array(
        'description' => 'The email address of the user leaving the contact message',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'subject' => array(
        'description' => 'The contact message subject',
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'message' => array(
        'description' => 'The contact message',
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'The date and time the message was left',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'opened' => array(
        'description' => 'When the message was opened',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'resolved' => array(
        'description' => 'When the message was resolved',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'foreign keys' => array(
      'category' => array(
        'table' => 'contact',
        'columns' => array('cid' => 'cid'),
      ),
      'mail_author' => array(
        'table' => 'users',
        'columns' => array('uid' => 'uid'),
      ),
    ),
    'primary key' => array('id'),
  );

  $schema['contact_centre_actions'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The ID of the message action',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'cid' => array(
        'description' => 'The ID of the contact message',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'uid' => array(
        'description' => 'The ID of the user carrying out the action',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'action' => array(
        'description' => 'The action recorded',
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ),
      'action_type' => array(
        'description' => 'The action type, note or reply',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'When the action was taken',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function contact_notes_install() {
  db_update('system')
    ->fields(array(
      'weight' => 100,
    ))
    ->condition('type', 'module')
    ->condition('name', 'contact_centre')
    ->execute();
}

/**
 * Implements hook_uninstall().
 */
function contact_centre_uninstall() {
  variable_del('contact_centre_show_unopened');
  variable_del('contact_centre_show_profile');
  variable_del('contact_centre_date_format');
  variable_del('contact_centre_messages_per_page');
}
